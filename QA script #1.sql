SELECT 
    country,
    country_code,
    item,
    FAO_item_code,
    year,
    A.value AS 'A.value',
    B.value AS 'B.value',
    A.value - B.value AS diff,
    ABS(A.value - B.value) AS absdiff,
    ABS(A.value - B.value) / A.value AS '%absdiff'
FROM
    area_17_postnfa_russia_ult.gaez A
        LEFT JOIN
    area_18_v3_ult.gaez B USING (country , country_code , item , FAO_item_code , year)
    WHERE country = 'Russian Federation'
ORDER BY ABS(A.value - B.value) / A.value DESC;