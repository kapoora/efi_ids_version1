SELECT 
    Year,
    country_code,
    Record,
    a.Country,
    a.Crop_Land,
    b.Crop_Land,
    a.Grazing_Land,
    b.Grazing_Land,
    a.Forest_Land,
    b.Forest_Land,
    a.Fishing_Ground,
    b.Fishing_Ground,
    a.Built_up_Land,
    b.Built_up_Land,
    a.Carbon,
    b.Carbon,
    a.Total,
    b.Total, 
    (b.Total - a.Total) AS diff
FROM
    footprint_18_caribbean.Summary_Results a
        LEFT JOIN
    footprint_18_ult8_world_final.summary_results b USING (year , country_code , Record); 