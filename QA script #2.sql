SELECT 
    country,
    country_code,
    year,
    trade_flow,
    FAO_item_code,
    item,
    A.weight AS 'A.weight',
    B.weight AS 'B.weight',
    A.weight - B.weight AS diff,
    ABS(A.weight - B.weight) AS absdiff,
    ABS(A.weight - B.weight) / A.weight AS '%absdiff'
FROM
    faostat_17_v1_ult.fao_livestock_weight A
        LEFT JOIN
    faostat_18_v2_ult.fao_livestock_weight B USING (country , country_code , year , trade_flow , FAO_item_code , item)
ORDER BY ABS(A.weight - B.weight) / A.weight DESC;