########################################################################################################
# Sample connection file and homework for IDS 
#
# install packages if needed
    install.packages("DBI")
    install.packages('RMySQL')
    install.packages('data.table')
    install.packages('ggplot2')
    install.packages('robustbase')
    install.packages('moments')
    install.packages('plyr')

# *COMMENT*
  require(DBI)
  library(RMySQL)
  library(data.table)
  library(ggplot2)
  library(robustbase)
  library(moments)
  library(plyr)

# Database Connection Configuration
  #list to hold connection parameters
  mysql<-list()

# Local Server - Comment/uncomment [ctrl-shift-c] whatever host you need
  # mysql$host <- "localhost"  # UPDATE
  # mysql$user <- "root"  # UPDATE
  # mysql$password <- "root"
  # mysql$port <- 3307 # UPDATE

# York Test Server - Comment/uncomment [ctrl-shift-c] whatever host you need
  mysql$host <- "nfa-york.cdz7fjttllpz.us-west-2.rds.amazonaws.com"  # UPDATE
  mysql$user <- "york"  # UPDATE
  mysql$password <- "gfn#york2018!"
  mysql$port <- 3306 # UPDATE

# Set the data source 
  input_schema<-'comtrade_19_baseline_v1_sql'
  input_table<-'comtrade_penult'

# Disconnect existing connections (should be done each run)
  all_cons <- dbListConnections(MySQL())
  if(length(all_cons) > 0) {
    for(i in 1:length(all_cons)){
      dbDisconnect(all_cons[[i]])
    }
  }
    
# Connect and Get data 
  ## mysql table `commodity_key` has the list of all commodities, 6612 is the code for cement
  db<-dbConnect(MySQL(), user=mysql$user, password=mysql$password, dbname=input_schema,host=mysql$host, port = mysql$port)
  query<-paste0("SELECT country_code, trade_flow, year, commodity_code, netweight_kg, trade_value, 
                trade_value/netweight_kg as ratio, log(trade_value/netweight_kg) as log_ratio
                FROM ",input_schema,".",input_table," 
                WHERE netweight_kg IS NOT NULL AND trade_value is not null
                AND trade_flow IN ('Import','Export')
                AND commodity_code = 6612
                ORDER BY country_code, year ASC")
  allData<-dbGetQuery(db,query)

  # Subset the data: looking at all of the data from the above query, for year 2000. 
  sample <- subset(allData, year == 2000)
  
  # Calculate the mean of the ratio for each country and the mean of the log_ratio for each country from the sample data.
  meanratio <-mean(sample$ratio)
  meanlogratio<-mean(sample$log_ratio)
  
  # Create a hitogram for the ratio of the sample data and a histogram for the log_ratio of the sample data.
  ggplot(sample, aes(x=ratio)) + geom_histogram()
  ggplot(sample, aes(x=log_ratio)) + geom_histogram()
  
  # Create a timeseries showing the trade_flow export data for the United Stated of America
  sampletimeseries<- subset(allData, trade_flow == "Export" & country_code == 33)
  
  # Plot seperate timeseries for trade_value,netweight_kg, ratio, and log_ratio from the sample data.
  ggplot(sampletimeseries, aes(year, trade_value)) + geom_line() + xlab("") + ylab("")
  ggplot(sampletimeseries, aes(year, netweight_kg)) + geom_line() + xlab("") + ylab("")
  ggplot(sampletimeseries, aes(year, ratio)) + geom_line() + xlab("") + ylab("")
  ggplot(sampletimeseries, aes(year, log_ratio)) + geom_line() + xlab("") + ylab("")
  
  # Creat a boxplot for the timeseries ratio and timeseries log_ratio from the sample data. 
  boxplot(sampletimeseries$ratio)
  boxplot(sampletimeseries$log_ratio)
  
  #Calculate the box-plot upper and lower threshold for outliers for each commodity code and data year
	#Add the thresholds to the "allData" table
  	adjbox.out <- ddply(allData, c("year","commodity_code"), summarize, 
		                    n = length(log_ratio), 
		                    lowerThreshold = boxplot.stats(log_ratio, coef = 3)$stats[1], 
		                    upperThreshold = boxplot.stats(log_ratio, coef = 3)$stats[5], 
		                    .progress = "text")

#Merge the calculated thresholds with all of the original data, allowing us to identify outliers on each line
		allResults <- merge(allData,adjbox.out, by=c("year","commodity_code"))
  
## Homework (Add to this script)
  # 1. Comment the above code everywhere it says *COMMENT*
  # 2. Make use of all tools (mysql database, excel, R) to identify and investigate a cement trade in Canada (world).
	#	3. Make a guess what what is going on.
  
  
  
